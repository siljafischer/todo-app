from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse

from .models import Entry

# Create your views here.

def index(request):
    entry_list = Entry.objects.all()
    context = {'entry_list' : entry_list}
    return render(request, 'todo/index.html', context)

def status(request, entry_id):
    entry = get_object_or_404(Entry, pk=entry_id)
    return render(request, 'todo/status.html', {'entry': entry})

def change(request, entry_id):
    entry = get_object_or_404(Entry, pk=entry_id)
    return render(request, 'todo/change.html', {'entry': entry})

def select(request, entry_id):
    entry = get_object_or_404(Entry, pk=entry_id)
    try: 
        done = request.POST['done'] == 'true'
        entry.done = done
        entry.save()
    except (KeyError):
        return render(request, 'todo/change.html', {'entry': entry, 'error':'Bitte Status wählen'})
    else:
        return redirect(reverse('todo:status', args=(entry_id,)))

def new_entry(request):
    entry_text = request.POST["entry_text"]
    Entry.objects.create(entry_text=entry_text)
    return HttpResponseRedirect(reverse('todo:index'))

