import datetime

from django.db import models
from django.utils import timezone

# Create your models here.

class Entry(models.Model):
    entry_text = models.CharField(max_length=200)
    done = models.BooleanField(default=False, null=False)
    def __str__(self):
        return self.entry_text




